# Yocto Project Example Build Setup for Jenkins

Contains a Jenkinsfile and supporting shell script to aid in building Poky.

## Building Without Jenkins

 1. install dependencies
 2. fetch Poky
 3. run build script

```
sudo ./scripts/setup.sh
./scripts/fetch.sh
./scripts/build.sh
```

> You can override the download and shared state file locations used
> during the build like this
>
> ```
> DL_DIR=/var/build/yocto/downloads SSTATE_DIR=/var/build/yocto/sstate-cache ./scripts/build.sh
> ```


## Known Issues

 * no kernel modules loaded - requires running ```depmod && reboot``` to fix (TODO automate it)
