# Beagle Bone Black TFTP Boot

## Development Host

On the development host

### Setup DHCP and TFTP

```
# Install dnsmasq
sudo apt-get -q install -y dnsmasq

# Become root
sudo su -

# Configure USB network interface
f=/etc/network/interfaces.d/eth-usb-asix
ifname=enx000ec6d8f129
echo "allow-hotplug $ifname" >> $f
echo "iface $ifname inet static" >> $f
echo -e "\taddress\t10.0.0.1" >> $f
echo -e "\tnetmask\t255.255.255.0" >> $f

ifup $ifname

# Create TFTP root directory
mkdir -p /var/lib/tftp
chown -R nobody:nobody /var/lib/tftp

# Create dnsmasq configuration
f=/etc/dnsmasq.d/bbb-tftpboot.conf
ecoh "interface=$ifname" >> $f
echo "dhcp-range=10.0.0.100,10.0.0.120" >> $f
echo "enable-tftp" >> $f
echo "tftp-root" >> $f

# Enable dnsmasq permanently and start the service
systemctl enable dnsmasq
systemctl start dnsmasq
```

After configuring network interface and dnsmasq, we need to copy a kernel, initrd and device tree to the tftp root directory.
The files could be taken from a build of Yocto Poky for the Beagle Bone. The files should be located in ```tmp/deploy/images/beaglebone-yocto``` of the respective build directory.

```
img_dir=/home/lazlo/src/poky/build-bbb/tmp/deploy/images/beaglebone-yocto
sudo cp zImage-beaglebone-yocto.bin am335x-boneblack.dtb /var/lib/tftp
sudo chown -R nobody:nobody /var/lib/tftp
```

### Setup NFS

```
sudo apt-get -q install -y nfs-kernel-server
sudo su -
rootfs="/home/lazlo/src/poky/build-bbb/tmp/work/beaglebone_yocto-poky-linux-gnueabi/core-image-minimal/1.0-r0/rootfs/"
echo "$rootfs 10.0.0.0/24(rw,no_root_squash)" >> /etc/exports
logout
sudo systemctl restart nfs-kernel-server
```

## Target Host

Stop the U-Boot boot process by pressing space and enter

```
setenv autload no
setenv pxefile_addr_r '0x50000000'
setenv kernel_addr_r '0x80200000'
setenv initrd_addr_r '0x81000000'
setenv fdt_addr_r '0x815f0000'
setenv initrd_high '0xffffffff'
setenv fdt_high '0xffffffff'

setenv loadkernel 'tftp ${kernel_addr_r} zImage-beaglebone-yocto.bin'
setenv loadinitrd 'tftp ${initrd_addr_r} <ramdisk-file-name>; setenv initrd_size ${filesize}'
setenv loadftd 'tftp ${fdt_addr_r} am335x-boneblack.dtb'
setenv bootargs 'console=ttyO0,115200n8 root=/dev/ram0 ip=:::::eth0:dhcp'
#setenv bootcmd 'dhcp; setenv serverip 10.0.0.1; run loadkernel; run loadinitrd; run loadftd; bootz ${kernel_addr_r} ${initrd_addr_r} ${ftd_addr_r}'

setenv bootcmd 'dhcp; setenv serverip 10.0.0.1; run loadkernel; run loadftd; bootz ${kernel_addr_r} ${initrd_addr_r} ${ftd_addr_r}'

setenv bootfile zImage-beaglebone-yocto.bin

boot
```
